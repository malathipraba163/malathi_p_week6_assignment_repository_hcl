package com.hcl.greatlearning.week6.bean;

public class Movies_In_Theaters {
	private int id;
	private String title;
	private int year;
	private String genres;
	private String duration;
	private int release_date;
	public Movies_In_Theaters() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Movies_In_Theaters(int id, String title, int year, String genres, String duration, int release_date) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genres = genres;
		this.duration = duration;
		this.release_date = release_date;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public int getRelease_date() {
		return release_date;
	}
	public void setRelease_date(int release_date) {
		this.release_date = release_date;
	}

	@Override
	public String toString() {
		return "Movies_In_Theaters [id=" + id + ", title=" + title + ", year=" + year + ", genres=" + genres
				+ ", duration=" + duration + ", release_date=" + release_date + "]";
	}
	
	
}
