package com.hcl.greatlearning.week6.bean;

public class Top_Rated_Movie {
	private String title;
	private int year;
	private String genres;
	private String duration;
	private int released_date;
	private int imdbRating ;
	
	
	public Top_Rated_Movie() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Top_Rated_Movie(String title, int year, String genres, String duration, int released_date, int imdbRating) {
		super();
		this.title = title;
		this.year = year;
		this.genres = genres;
		this.duration = duration;
		this.released_date = released_date;
		this.imdbRating = imdbRating;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public int getReleased_date() {
		return released_date;
	}
	public void setReleased_date(int released_date) {
		this.released_date = released_date;
	}
	public int getImdbRating() {
		return imdbRating;
	}
	public void setImdbRating(int imdbRating) {
		this.imdbRating = imdbRating;
	}

	@Override
	public String toString() {
		return "Top_Rated_Movie [title=" + title + ", year=" + year + ", genres=" + genres + ", duration=" + duration
				+ ", released_date=" + released_date + ", imdbRating=" + imdbRating + "]";
	}
	
	


}
