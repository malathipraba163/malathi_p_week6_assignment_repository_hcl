package com.hcl.greatlearning.week6.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hcl.greatlearning.week6.bean.Movie_Comming;
import com.hcl.greatlearning.week6.bean.Movies_In_Theaters;
import com.hcl.greatlearning.week6.bean.Top_Rated_Indianmovie;
import com.hcl.greatlearning.week6.bean.Top_Rated_Movie;
import com.hcl.greatlearning.week6.resource.DbResource;

public class MovieDao{
	

	public List<Movie_Comming> findAllProdcut() {
		List<Movie_Comming> listOfMovie = new ArrayList<>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from product");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					Movie_Comming p = new Movie_Comming();
					p.setId(rs.getInt(1));
					p.setTitle(rs.getString(2));
					p.setYear(rs.getInt(3));
					p.setGenres(rs.getString(4));
					p.setDuration(rs.getString(5));
					p.setRelease_date(rs.getInt(6));
					
					listOfMovie.add(p);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfMovie;
	}
	public List<Movies_In_Theaters> findAllMovie() {
		List<Movies_In_Theaters> listOfMovie = new ArrayList<>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from product");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				    Movies_In_Theaters M = new Movies_In_Theaters();
					M.setId(rs.getInt(1));
					M.setTitle(rs.getString(2));
					M.setYear(rs.getInt(3));
					M.setGenres(rs.getString(4));
					M.setDuration(rs.getString(5));
					M.setRelease_date(rs.getInt(6));
					
					listOfMovie.add(M);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfMovie;
	}
	public List<Top_Rated_Indianmovie> findAllMovies() {
		List<Top_Rated_Indianmovie> listOfMovie = new ArrayList<>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from product");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				    Top_Rated_Indianmovie Mr = new Top_Rated_Indianmovie();
					Mr.setTitle(rs.getString(1));
					Mr.setYear(rs.getInt(2));
					Mr.setGenres(rs.getString(3));
					Mr.setDuration(rs.getString(4));
					Mr.setRelease_date(rs.getInt(5));
					
					listOfMovie.add(Mr);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfMovie;
	}
	public List<Top_Rated_Movie> findMovies() {
		List<Top_Rated_Movie> listOfMovie = new ArrayList<>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from product");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				    Top_Rated_Movie tr = new Top_Rated_Movie();
					tr.setTitle(rs.getString(1));
					tr.setYear(rs.getInt(2));
					tr.setGenres(rs.getString(3));
					tr.setDuration(rs.getString(4));
					tr.setReleased_date(rs.getInt(5));
					
					listOfMovie.add(tr);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfMovie;
	}
}
