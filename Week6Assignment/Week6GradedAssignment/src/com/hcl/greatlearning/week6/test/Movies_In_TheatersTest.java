package com.hcl.greatlearning.week6.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hcl.greatlearning.week6.bean.Movie_Comming;
import com.hcl.greatlearning.week6.bean.Movies_In_Theaters;

public class Movies_In_TheatersTest {

	@Test
	public void testGetId() {
		System.out.println("getId");
		Movies_In_Theaters instance=new Movies_In_Theaters();
		int expresult=1;
		instance.setId(1);
		int result=instance.getId();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetId() {
		System.out.println("setId");
		int setId=1;
		Movies_In_Theaters instance=new Movies_In_Theaters();
		instance.setId(1);
		assertEquals(instance.getId(), setId);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		System.out.println("getTitle");
		Movies_In_Theaters instance=new Movies_In_Theaters();
		String expresult="Samson";
		instance.setTitle("Samson");
		String result=instance.getTitle();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		System.out.println("setTitle");
		String setTitle="Samson";
		Movies_In_Theaters instance=new Movies_In_Theaters();
		instance.setTitle("Samson");
		assertEquals(instance.getTitle(), setTitle);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		System.out.println("getYear");
		Movies_In_Theaters instance=new Movies_In_Theaters();
		int expresult=2012;
		instance.setYear(2012);
		int result=instance.getYear();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		System.out.println("setYear");
		int setYear=2018;
		Movies_In_Theaters instance=new Movies_In_Theaters();
		instance.setYear(2018);
		assertEquals(instance.getYear(), setYear);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetGenres() {
		System.out.println("getGenres");
		Movies_In_Theaters instance=new Movies_In_Theaters();
		String expresult="Drama";
		instance.setGenres("Drama");
		String result=instance.getGenres();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetGenres() {
		System.out.println("setGenres");
		String setGenres="Drama";
		Movies_In_Theaters instance=new Movies_In_Theaters();
		instance.setGenres("Drama");
		assertEquals(instance.getGenres(),setGenres);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetDuration() {
		System.out.println("getDuration");
		Movies_In_Theaters instance=new Movies_In_Theaters();
		String expresult="PT80L";
		instance.setDuration("PT80L");
		String result=instance.getDuration();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetDuration() {
		System.out.println("setDuration");
		String setDuration="PT80L";
		Movies_In_Theaters instance=new Movies_In_Theaters();
		instance.setDuration("PT80L");
		assertEquals(instance.getDuration(),setDuration);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetRelease_date() {
		System.out.println("getRelease_date");
		Movies_In_Theaters instance=new Movies_In_Theaters();
		int expresult=2015-01-23;
		instance.setRelease_date(2015-01-23);
		int result=instance.getRelease_date();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetRelease_date() {
		System.out.println("setRelease_date");
		int setRelease_date=2015-01-23;
		Movies_In_Theaters instance=new Movies_In_Theaters();
		instance.setRelease_date(2015-01-23);
		assertEquals(instance.getRelease_date(), setRelease_date);
		//fail("Not yet implemented");
	}

}
