package com.hcl.greatlearning.week6.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hcl.greatlearning.week6.bean.Movie_Comming;
import com.hcl.greatlearning.week6.bean.Top_Rated_Indianmovie;

public class Top_Rated_IndianmovieTest {

	@Test
	public void testGetTitle() {                                   //To Test Top_Rated_IndianMovie Class using getter and setting method 
		System.out.println("getTitle");
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		String expresult="Anbe sivam";
		instance.setTitle("Anbe sivam");
		String result=instance.getTitle();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		System.out.println("setTitle");
		String setTitle="Anbe sivam";
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		instance.setTitle("Anbe sivam");
		assertEquals(instance.getTitle(), setTitle);
		//fail("Not yet implemented");
	}
    @Test
	public void testGetYear() {
    	System.out.println("getYear");
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		int expresult=2003;
		instance.setYear(2003);
		int result=instance.getYear();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		System.out.println("setYear");
		int setYear=2003;
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		instance.setYear(2003);
		assertEquals(instance.getYear(), setYear);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testGetGenres() {
		System.out.println("getGenres");
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		String expresult="Drama";
		instance.setGenres("Drama");
		String result=instance.getGenres();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetGenres() {
		System.out.println("setGenres");
		String setGenres="Drama";
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		instance.setGenres("Drama");
		assertEquals(instance.getGenres(),setGenres);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetDuration() {
		System.out.println("getDuration");
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		String expresult="PT190K";
		instance.setDuration("PT190K");
		String result=instance.getDuration();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetDuration() {
		System.out.println("setDuration");
		String setDuration="PT190K";
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		instance.setDuration("PT190K");
		assertEquals(instance.getDuration(),setDuration);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetRelease_date() {
		System.out.println("getRelease_date");
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		int expresult=2005-02-15;
		instance.setRelease_date(2005-02-15);
		int result=instance.getRelease_date();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetRelease_date() {
		System.out.println("setRelease_date");
		int setRelease_date=2005-02-15;
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		instance.setRelease_date(2005-02-15);
		assertEquals(instance.getRelease_date(), setRelease_date);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetImdbRating() {
		System.out.println("getImdbRating");
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		int expresult=9;
		instance.setRelease_date(9);
		int result=instance.getRelease_date();
		assertEquals(expresult,result);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetImdbRating() {
		System.out.println("setImdbRating");
		int setRelease_date=9;
		Top_Rated_Indianmovie instance=new Top_Rated_Indianmovie();
		instance.setRelease_date(9);
		assertEquals(instance.getRelease_date(), setRelease_date);
		
		//fail("Not yet implemented");
	}

}
