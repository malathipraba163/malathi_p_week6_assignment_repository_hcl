package com.hcl.greatlearning.week6.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hcl.greatlearning.week6.bean.Movie_Comming;

public class Movie_CommingTest {                      //To Test Movie_Comming bean  class 

	@Test
	public void testGetId() {
		System.out.println("getId");
		Movie_Comming instance=new Movie_Comming();
		int expresult=1;
		instance.setId(1);
		int result=instance.getId();
		assertEquals(expresult,result);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetId() {
		System.out.println("setId");
		int setId=1;
		Movie_Comming instance=new Movie_Comming();
		instance.setId(1);
		assertEquals(instance.getId(), setId);
		//fail("Not yet implemented");
	}

	
	@Test
	public void testGetTitle() {
		System.out.println("getTitle");
		Movie_Comming instance=new Movie_Comming();
		String expresult="Game Night";
		instance.setTitle("Game Night");
		String result=instance.getTitle();
		assertEquals(expresult,result);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		System.out.println("setTitle");
		String setTitle="Area X Annihilation";
		Movie_Comming instance=new Movie_Comming();
		instance.setTitle("Area X Annihilation");
		assertEquals(instance.getTitle(), setTitle);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		System.out.println("getYear");
		Movie_Comming instance=new Movie_Comming();
		int expresult=2018;
		instance.setYear(2018);
		int result=instance.getYear();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		System.out.println("setYear");
		int setYear=2018;
		Movie_Comming instance=new Movie_Comming();
		instance.setYear(2018);
		assertEquals(instance.getYear(), setYear);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testGetGenres() {
		System.out.println("getGenres");
		Movie_Comming instance=new Movie_Comming();
		String expresult="Action";
		instance.setGenres("Action");
		String result=instance.getGenres();
		assertEquals(expresult,result);
		
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetGenres() {
		System.out.println("setGenres");
		String setGenres="Action";
		Movie_Comming instance=new Movie_Comming();
		instance.setGenres("Action");
		assertEquals(instance.getGenres(),setGenres);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testGetDuration() {
		System.out.println("getDuration");
		Movie_Comming instance=new Movie_Comming();
		String expresult="PT100M";
		instance.setDuration("PT100M");
		String result=instance.getDuration();
		assertEquals(expresult,result);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetDuration() {
		System.out.println("setDuration");
		String setDuration="PT100M";
		Movie_Comming instance=new Movie_Comming();
		instance.setDuration("PT100M");
		assertEquals(instance.getDuration(),setDuration);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetRelease_date() {
		System.out.println("getRelease_date");
		Movie_Comming instance=new Movie_Comming();
		int expresult=2018-02-23;
		instance.setRelease_date(2018-02-23);
		int result=instance.getRelease_date();
		assertEquals(expresult,result);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetRelease_date() {
		System.out.println("setRelease_date");
		int setRelease_date=2018-02-23;
		Movie_Comming instance=new Movie_Comming();
		instance.setRelease_date(2018-02-23);
		assertEquals(instance.getRelease_date(), setRelease_date);
		//fail("Not yet implemented");
	}

	}
