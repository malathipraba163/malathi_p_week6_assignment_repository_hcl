package com.hcl.greatlearning.week6.test;

import static org.junit.Assert.*;

import org.junit.Test;
import com.hcl.greatlearning.week6.bean.Top_Rated_Movie;

public class Top_Rated_MovieTest {

	@Test
	public void testGetTitle() {
		System.out.println("getTitle");
		Top_Rated_Movie instance=new Top_Rated_Movie();
		String expresult="A Beautiful Mind";
		instance.setTitle("A Beautiful Mind");
		String result=instance.getTitle();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		System.out.println("setTitle");
		String setTitle="A Beautiful Mind";
		Top_Rated_Movie instance=new Top_Rated_Movie();
		instance.setTitle("A Beautiful Mind");
		assertEquals(instance.getTitle(), setTitle);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		System.out.println("getYear");
		Top_Rated_Movie instance=new Top_Rated_Movie();
		int expresult=2001;
		instance.setYear(2001);
		int result=instance.getYear();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		System.out.println("setYear");
		int setYear=2001;
		Top_Rated_Movie instance=new Top_Rated_Movie();
		instance.setYear(2001);
		assertEquals(instance.getYear(), setYear);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetGenres() {
		System.out.println("getGenres");
		Top_Rated_Movie instance=new Top_Rated_Movie();
		String expresult="Biography";
		instance.setGenres("Biography");
		String result=instance.getGenres();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetGenres() {
		System.out.println("setGenres");
		String setGenres="Biography";
		Top_Rated_Movie instance=new Top_Rated_Movie();
		instance.setGenres("Biography");
		assertEquals(instance.getGenres(),setGenres);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetDuration() {
		System.out.println("getDuration");
		Top_Rated_Movie instance=new Top_Rated_Movie();
		String expresult="MS00S";
		instance.setDuration("MS00S");
		String result=instance.getDuration();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetDuration() {
		System.out.println("setDuration");
		String setDuration="MS00S";
		Top_Rated_Movie instance=new Top_Rated_Movie();
		instance.setDuration("MS00S");
		assertEquals(instance.getDuration(),setDuration);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetReleased_date() {
		System.out.println("getRelease_date");
		Top_Rated_Movie instance=new Top_Rated_Movie();
		int expresult=2001-02-15;
		instance.setReleased_date(2001-02-15);
		int result=instance.getReleased_date();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetReleased_date() {
		System.out.println("setRelease_date");
		int setRelease_date=2005-02-15;
		Top_Rated_Movie instance=new Top_Rated_Movie();
		instance.setReleased_date(2005-02-15);
		assertEquals(instance.getReleased_date(), setRelease_date);
		//fail("Not yet implemented");
	}

}
