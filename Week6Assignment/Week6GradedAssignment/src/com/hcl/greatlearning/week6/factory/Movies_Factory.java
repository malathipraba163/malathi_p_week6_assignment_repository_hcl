package com.hcl.greatlearning.week6.factory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hcl.greatlearning.week6.bean.Movie_Comming;
import com.hcl.greatlearning.week6.bean. Movies_In_Theaters;
import com.hcl.greatlearning.week6.bean. Top_Rated_Indianmovie;
import com.hcl.greatlearning.week6.bean.Top_Rated_Movie;
import com.hcl.greatlearning.week6.resource.DbResource;


 interface MovieTips  {
	
	public List<String> movieType() throws Exception ;              

}
class MovieComingSoon implements MovieTips{
	Connection con=DbResource.getDbConnection();
	public MovieComingSoon() {}
	@Override
	public List<String> movieType() {
		
		try {
			List<Movie_Comming> movies=new ArrayList<Movie_Comming>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movie_Comming");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Movie_Comming movie=new Movie_Comming();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setRelease_date(rs.getInt(6));
				movies.add(movie);
			}}catch(Exception e) {
				System.out.println("Movie_Comming"+e);
			}
			
		
		return null;
	}
	
}
class MovieTheatres implements MovieTips{
	Connection con=DbResource.getDbConnection();
	public MovieTheatres() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Movies_In_Theaters> movies=new ArrayList<Movies_In_Theaters>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movies_In_Theaters"); //Retrieve the records using Movies_In_theatres
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Movies_In_Theaters movie=new Movies_In_Theaters();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setRelease_date(rs.getInt(6));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Movies_In_Theaters"+e);
		}
	    return null;	
	}
	
}
class Top_Rated implements MovieTips{
	Connection con=DbResource.getDbConnection();
	public Top_Rated() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Top_Rated_Indianmovie> movies=new ArrayList<Top_Rated_Indianmovie>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Indianmovie"); //Retrieve the records
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Top_Rated_Indianmovie movie=new Top_Rated_Indianmovie();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenres(rs.getString(3));
				movie.setDuration(rs.getString(4));
				movie.setRelease_date(rs.getInt(5));
				movie.setImdbRating(rs.getInt(6));
				
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Indianmovi"+e);
		}
	    return null;	
	}
	
	
}
class Rated_Movie implements MovieTips{
	Connection con=DbResource.getDbConnection();
	public Rated_Movie() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Top_Rated_Movie> movies=new ArrayList<Top_Rated_Movie>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Movie");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Top_Rated_Movie movie=new Top_Rated_Movie();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setReleased_date(rs.getInt(6));
				movie.setImdbRating(rs.getInt(7));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Movie"+e);
		}
	    return null;	
	}
}
	
class MovieTipsFactory{
	public static MovieTips getInstance(String type) {
		if(type.equalsIgnoreCase("MovieComingSoon")) {
			return new MovieComingSoon();
		}else if(type.equalsIgnoreCase("MovieTheatres")) {
			return new MovieTheatres();
		}else if(type.equalsIgnoreCase("Top_Rated")) {
			return new Top_Rated();
		}
		else if(type.equalsIgnoreCase("Rated_Movie")) {
			return new Rated_Movie();
		}else {
	    	return null;
	}
}
}
    public class Movies_Factory{
	public void main(String[] args) throws Exception {
		MovieTips dd1 =  MovieTipsFactory.getInstance("MovieComingSoon");
		dd1.movieType();
	    MovieTips dd2 = MovieTipsFactory.getInstance("MovieTheatres");
	    dd2.movieType();
	    MovieTips dd3 = MovieTipsFactory.getInstance("Top_Rated");
	    dd3.movieType();
	    MovieTips dd4=MovieTipsFactory.getInstance("Rated_Movie");
	    dd4.movieType();
				
		
	}

    }

